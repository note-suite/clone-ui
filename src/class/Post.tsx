export class Post {
    id: number = 0;
    title: String = "default title";

    constructor(id: number, title: String) {
        this.id = id;
        this.title = title;
    }
};