import { Box, HStack, Text } from "@chakra-ui/react";
import React from 'react';

import { Post } from "../class/Post";

interface Props {
  post: Post;
}

const PostCard = ({ post }: Props) => {
    return (
      <HStack key={post.id} w="100%" alignItems="flex-start">
        <Box bg="gray.100" p={4} rounded="md" w="100%">
          <Text>{post.title}</Text>
        </Box>
      </HStack>
    );
  };
  
export default PostCard;