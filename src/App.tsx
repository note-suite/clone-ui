import React, { useEffect, useState } from "react";
import './App.css';
import PostCard from './components/PostCard';
import { ChakraProvider, Container, VStack } from "@chakra-ui/react";
import { Post } from "./class/Post";

function App() {
  const [posts, setPosts] = useState([new Post(1, "default title"), new Post(2, "second default title")]);
  return (
    <Container maxW="lg" centerContent p={8}>
      <VStack spacing={2} w="100%">
        {posts.map((post: Post) => (
          <PostCard post={post} key={post.id}/>
        ))}
      </VStack>
    </Container>
  );
}

export default App;
